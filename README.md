# Telegram bridge for Gancio

This is an attempt to create a plugin for Gancio that republishes content to Telegram channels or groups. The goal is to spread the info of our networks to the capitalist cyberspace, and pull otherwise isolated people to our radical and free part of the internet.

## Clone this repo to your Gancio

```bash
# assuming docker installation as per https://gancio.org/install/docker
$ cd /opt/gancio/data
$ mkdir -p plugins
$ cd plugins
$ git clone https://framagit.org/bcn.convocala/gancio-plugin-telegram-bridge.git
$ cd gancio-plugin-telegram-bridge 
$ npm i
```

## Configuration


Once the plugin is installed, navigate to your instance plugins tab of the admin interface. Enable the plugin, and add the required data: 
* Your Telegram's **bot token** you want to impersonate with this plugin. [Help on telegram bots](https://core.telegram.org/bots#6-botfather)
* The **channel/group id** where to publish the messages. [Help on getting chat ids](https://github.com/GabrielRF/telegram-id)
* Decide if you want tags to link to your Gancio instance, or let them be Telegram hashtags
* Decide if you want to append a link to your Gancio instance in every event message


## Try it

1. Restart your gancio instance and look at the logs for any message saying that this plugin has been loaded.
2. Edit an existing event and save it just like that (this counts as an update)
3. Check your telegram channel for new activities

